/*
* Name: deck.cpp
* Author: Zach Miller (zach.miller@wustl.edu)
* Description: This file contains the definitions for the Deck class.  Decks have a vector containing Card objects.  Cards are inserted into the deck with the addCardsFromFile function. The deck
* can be shuffled using the shuffleDeck function
*/


#pragma once

#include "stdafx.h"
#include "deck.h"
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <map>
#include <random>
#include <algorithm>

using namespace std;

const int success = 0;  //value returned if a function doesn't run into any errors

						//These 2 maps are used by the output stream insertion operator.  The integer values for the suit and rank are mapped to the appropriate strings
map<int, string> rankMap = {
	{ 2,  "2" },
{ 3,  "3" },
{ 4,  "4" },
{ 5,  "5" },
{ 6,  "6" },
{ 7,  "7" },
{ 8,  "8" },
{ 9,  "9" },
{ 10, "10" },
{ 11, "J" },
{ 12, "Q" },
{ 13, "K" },
{ 14, "A" }
};

map<int, string> suitMap = {
	{ 0, "C" },
{ 1, "D" },
{ 2, "H" },
{ 3, "S" }
};


/*Deck constructor:  if the file cannot be opened, throw an exception*/
Deck::Deck(char * filename) {
	try {
		addCardsFromFile(filename);
	}
	catch (char * e) {
		throw e;
	}
}

/*This function parses words from an file, and adds cards to the member variable if it comes across a valid card definition string*/
int Deck::addCardsFromFile(char * filename) {

	//create a file stream and open the file
	ifstream file;
	file.open(filename);

	//Check if the function was able to open the file.  If not, throw an exception.
	if (!file.is_open()) {
		throw "Unable to open file";
	}


	string line;               //variable used to store each line of the file
	vector<Card> tempVector;   //temporary vector to add cards to.  If a hand is valid, the cards in this vector get copied to the actual vector, otherwise it gets cleared

							   //this while-loop reads the file line-by-line
	while (getline(file, line)) {

		istringstream lineStream(line);   //place the line into a string stream
		unsigned int numberOfValidCards = 0;       //variable to keep track of the number of valid cards in a line
		tempVector.clear();               //clear the temp vector at the start of every new line

		Card::Suit suit;     //These variables will be initialized if the word is successfully parsed
		Card::Rank rank;

		bool validCard = true;   //Boolean to keep track of whether or not the word is a valid card

		int suitIndex;           //used when parsing the number 10: If the number is 10, the suit is checked on the 3rd character, otherwise the suit is
								 //checked on the second character of the word
		string word;

		//this while-loop reads the string word-by-word
		while (lineStream >> word)
		{
			//stop reading the line if you come across a '//'
			if (word[0] == '/') {
				if (word[1] == '/') {
					break;
				}
			}

			//This if/elseif checks the length of the string, and ignores it if the length is not 2 or 3
			if (word.length() == 2) {
				suitIndex = 1;
			}
			else if (word.length() == 3) {
				suitIndex = 2;
			}
			else {
				continue;
			}

			/*
			*This switch statement checks the first character of the word, and assigns the appropriate rank.  If the first character is a '1',
			*it checks the second character to see if it's a '0'
			*/
			switch (word.at(0)) {
			case '1':
				if (word.at(1) == '0') {
					rank = Card::ten;
				}
				else {
					continue;
				}
				break;
			case '2':
				rank = Card::two;
				break;
			case '3':
				rank = Card::three;
				break;
			case '4':
				rank = Card::four;
				break;
			case '5':
				rank = Card::five;
				break;
			case '6':
				rank = Card::six;
				break;
			case '7':
				rank = Card::seven;
				break;
			case '8':
				rank = Card::eight;
				break;
			case '9':
				rank = Card::nine;
				break;
			case 'j':
			case 'J':
				rank = Card::jack;
				break;
			case 'q':
			case 'Q':
				rank = Card::queen;
				break;
			case 'k':
			case 'K':
				rank = Card::king;
				break;
			case 'a':
			case 'A':
				rank = Card::ace;
				break;
			default:
				validCard = false;
				break;
			}

			//This if statement is used to ignore inputs such as '2CC'.
			if (rank != Card::ten && suitIndex == 2) {
				continue;
			}

			/*
			*Switch statement that determines the suit.  If the rank is 10 (i.e. suitIndex=2), look at the 3rd character of the word, otherwise look at the
			*second character (suitIndex=1)
			*/
			switch (word.at(suitIndex)) {
			case 'c':
			case 'C':
				suit = Card::clubs;
				break;
			case 'd':
			case 'D':
				suit = Card::diamonds;
				break;
			case 's':
			case 'S':
				suit = Card::spades;
				break;
			case 'h':
			case 'H':
				suit = Card::hearts;
				break;
			default:
				validCard = false;
			}

			//only create the card and add it to the vector if it is valid.  
			if (validCard) {
				Card c = { suit, rank };
				this->cards.push_back(c);
			}
			validCard = true;
		}

	}
	return success;
}

/*Output stream insertion operator: Use the maps to convert the ints to strings, then insert those strings into the ostream*/
ostream & operator<<(std::ostream& out, const Deck & deck) {
	Deck d = deck;

	//print a message and return if the deck is empty
	if (d.size() == 0) {
		cout << "Empty Deck" << endl;
		return out;
	}
	for (size_t i = 0; i != d.size(); ++i) {
		int suit = d.cards[i].suit;
		int rank = d.cards[i].rank;
		cout << rankMap.find(rank)->second << suitMap.find(suit)->second << endl;
	}

	return out;
}

/*This method shuffles the cards in the deck*/
void Deck::shuffleDeck() {
	std::random_device rd;
	std::mt19937 g(rd());
	std::shuffle(this->cards.begin(), this->cards.end(), g);
}

/*Returns the size of the member variable, i.e. the number of cards in the deck*/
const int Deck::size() {
	return this->cards.size();
}

/*getter method that returns the cards in the deck*/
vector<Card> & Deck::getCards() {
	return this->cards;
}


void Deck::add_card(Card c) {
	this->cards.push_back(c);
}

