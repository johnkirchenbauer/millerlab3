#ifndef PLAYER_H
#define PLAYER_H

#include "hand.h"


struct Player {
	Player(char * name);


	char * playerName;
	Hand player_cards;
	unsigned int hands_won;
	unsigned int hands_lost;
};

ostream & operator<<(ostream & out, const Player & p);


#endif